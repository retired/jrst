.. -
.. * #%L
.. * JRst :: Documentation
.. * %%
.. * Copyright (C) 2009 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====================
Various librairy used
=====================

.. contents::

dom4j_
======

Dom4j_ is an easy to use, open source library for working with XML, XPath and XSLT on the Java platform
using the Java Collections Framework and with full support for DOM, SAX and JAXP.

Saxon_
======

Saxon_ is an open source XSLT processor for Java. In this project, it is used to apply XSL stylesheets to XML.

IText_
======

IText_ is a library to generate PDF documents from (x)HTML pages.

Xmlunit_
========

Xmlunit_ used to compare two XML files to show their differences.

DocUtils_
=========

DocUtils_ is a set of Python scripts to transform RST_ files into useful formats, such as HTML, ODT, Latex and more.

Jython_
=======

Jython_ is a Python interpreter for Java. Here, it allows to execute DocUtils_ scripts.



.. _dom4j: http://www.dom4j.org
.. _Saxon: http://java.sun.com/javase/6/docs/api/javax/xml/transform/package-summary.html
.. _IText: http://itextpdf.com/
.. _Xmlunit: http://xmlunit.sourceforge.net/
.. _RST: http://docutils.sourceforge.net/rst.html
.. _DocUtils: http://docutils.sourceforge.net/docs/ref/doctree.html
.. _Jython: http://jython.org/index.html
