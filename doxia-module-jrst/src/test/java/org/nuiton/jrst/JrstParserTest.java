/*
 * #%L
 * JRst :: Doxia module
 * %%
 * Copyright (C) 2009 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import org.apache.maven.doxia.AbstractModuleTest;
import org.apache.maven.doxia.module.xdoc.XdocSink;
import org.apache.maven.doxia.parser.Parser;
import org.apache.maven.doxia.sink.Sink;

import java.io.File;
import java.io.Reader;
import java.io.StringWriter;

/**
 * @author chatellier
 * @version $Revision : 1$
 */
public class JrstParserTest extends AbstractModuleTest {

    protected JrstParser parser;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        parser = lookup(Parser.ROLE, "jrst");

        // strange behaviour since surefire update
        new File(System.getProperty("java.io.tmpdir")).mkdirs();
    }

    public void testParse() throws Exception {
        Reader reader = null;

        try {
            StringWriter output = new StringWriter();
            reader = getTestReader("test", "rst");

            Sink sink = new XdocSink(output) {
            };
            parser.parse(reader, sink);

            String outputResult = output.toString();

            assertTrue(outputResult.contains("emphasis"));
            assertTrue(outputResult.contains("This is the first item"));
            assertTrue(outputResult.contains("Title"));
            assertTrue(outputResult.contains("blocks."));
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    @Override
    protected String outputExtension() {
        return "rst";
    }

    @Override
    protected String getOutputDir() {
        return "parser/";
    }

}
