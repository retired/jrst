/*
 * #%L
 * JRst :: Doxia module
 * %%
 * Copyright (C) 2009 - 2022 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import org.apache.maven.doxia.parser.Parser;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;

/**
 * Parse an RST model, transform it into xdoc model and emit events into the
 * specified doxia Sink.
 *
 * @author <a href="mailto:chatellier@codelutin.com">Eric Chatellier</a>
 * @version $Id$
 * @since 2.0
 */
@Component(role = Parser.class, hint = "jrst",
           description = "Parse an RST model (using python + docutils), " +
                         "transform it into xdoc model and emit events " +
                         "into the specified doxia Sink.")
public class JrstParser extends AbstractJrstParser {

    @Requirement(hint = "docutils")
    protected JRSTToXmlStrategy jrstStrategy;

    @Override
    public JRSTToXmlStrategy getStrategy() {
        return jrstStrategy;
    }
}
