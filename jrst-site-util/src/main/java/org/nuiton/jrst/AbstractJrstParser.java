/*
 * #%L
 * JRst :: Site util
 * %%
 * Copyright (C) 2012 - 2022 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.maven.doxia.logging.Log;
import org.apache.maven.doxia.module.xdoc.XdocParser;
import org.apache.maven.doxia.parser.AbstractParser;
import org.apache.maven.doxia.parser.ParseException;
import org.apache.maven.doxia.parser.Parser;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.siterenderer.RenderingContext;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.util.IOUtil;
import org.dom4j.Document;
import org.nuiton.util.FileUtil;

/**
 * Abstract Jrst doxia parser.
 *
 * @author tchemit (chemit@codelutin.com)
 * @since 2.0.1
 */
public abstract class AbstractJrstParser extends AbstractParser {

    public static final String JRST_PARSER_ID = "jrst";

    public abstract JRSTToXmlStrategy getStrategy();

    protected RenderingContext renderingContext;

    protected MavenProject mavenProject;

    protected boolean verbose;

    @Requirement(role = Parser.class, hint = "xdoc")
    protected XdocParser xdocParser;

    @Override
    public void parse(Reader source, Sink sink) throws ParseException {

        try {
            // Write the source in a file to use it with JRST
            File sourceFile = prepareSourceFile(getLog(), source);

            // Generation of the xml file
//            Document doc = JRST.generateRstToXml(sourceFile);
            Document doc = getStrategy().generateRstToXml(sourceFile, JRST.UTF_8);

            // Application of xsl stylesheets
            doc = JRST.generateXml(doc, JRST.TYPE_XDOC);

            // Give xsl result to XDoc parser
            Reader reader = new StringReader(doc.asXML());
            xdocParser.parse(reader, sink);
        } catch (Exception e) {
            throw new ParseException("Can't parse rst file", e);
        } finally {

            clear();
        }
    }

    @Override
    public void parse(Reader source, Sink sink, String reference) throws ParseException {
        parse(source, sink);
    }

    public void setRenderingContext(RenderingContext renderingContext) {
        this.renderingContext = renderingContext;
    }

    public void setMavenProject(MavenProject mavenProject) {
        this.mavenProject = mavenProject;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    protected File prepareSourceFile(Log log, Reader source) throws IOException {

        File sourceFile;

        if (mavenProject == null) {
            sourceFile = File.createTempFile("source", "rst");

        } else {
            File tempDirectory = new File(
                    mavenProject.getBasedir(),
                    "target" + File.separator + "generated-jrst");

            sourceFile = new File(tempDirectory, renderingContext.getInputName());

            FileUtil.createDirectoryIfNecessary(sourceFile.getParentFile());
        }

        if (verbose) {
            log.info("Transform rst file: " + sourceFile);
            if (log.isDebugEnabled()) {
                log.info("Copy " + renderingContext.getInputName() +
                         " to  " + sourceFile);
            }
        }

        FileWriter fileWriter = new FileWriter(sourceFile);
        try {
            IOUtil.copy(source, fileWriter);
        } finally {
            fileWriter.close();
        }

        return sourceFile;
    }

    public void clear() {
        renderingContext = null;
        mavenProject = null;
        verbose = false;
    }

}
