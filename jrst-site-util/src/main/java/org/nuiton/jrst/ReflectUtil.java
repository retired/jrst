/*
 * #%L
 * JRst :: Site util
 * %%
 * Copyright (C) 2006 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Some usefull methods to obtains none accessible fields and method from
 * some classes.
 *
 * @author tchemit (chemit@codelutin.com)
 * @since 1.0.6
 */
public class ReflectUtil {

    public static Method getMethod(Class<?> klass, String methodName, Class<?>[] paramTypes) {
        try {
            Method declaredMethod = klass.getDeclaredMethod(methodName, paramTypes);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Exception e) {
            throw new IllegalStateException("Could not get method [" + methodName + "] from parent class :(... ", e);
        }

    }

    public static Field getField(Class<?> klass, String fieldName) {
        try {
            Field declaredField = klass.getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Exception e) {
            throw new IllegalStateException("Could not get field [" + fieldName + "] from parent class :(... ", e);
        }

    }

    public static <O> O invokeMethod(Method m, Object o, Object... params) {
        try {
            O result = (O) m.invoke(o, params);
            return result;
        } catch (Exception e) {
            throw new IllegalStateException("Could not invoke method [" + m.getName() + "] from parent class :(... ", e);
        }

    }
}
