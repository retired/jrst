/*
 * #%L
 * JRst :: Site util
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import org.apache.maven.doxia.parser.module.AbstractParserModule;
import org.apache.maven.doxia.parser.module.ParserModule;
import org.codehaus.plexus.component.annotations.Component;

/**
 * The jrst site module.
 *
 * @author tchemit (chemit@codelutin.com)
 * @since 2.0.1
 */
@Component(role = ParserModule.class, hint = AbstractJrstParser.JRST_PARSER_ID,
           description = "Jrst doxia site mdule using jrst for rst.")
public class JrstSiteModule extends AbstractParserModule {

    @Override
    public String getSourceDirectory() {
        return "rst";
    }

    @Override
    public String[] getExtensions() {
        return new String[]{"rst"};
    }

    @Override
    public String getParserId() {
        return AbstractJrstParser.JRST_PARSER_ID;
    }
}
