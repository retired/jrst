/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.ui;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.List;
import org.nuiton.util.StringUtil;
import org.python.google.common.collect.Lists;

/**
 * Created: 08/06/12
 *
 * @author jpages
 */
public class JRSTCommandModel {
    protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    protected Boolean overwrite = Boolean.FALSE;
    protected Boolean simpleMode = Boolean.FALSE;
    protected Boolean formatEnabled = Boolean.TRUE;

    protected File saveFile;
    protected File openFile;
    protected String selectedFormat;

    protected List<File> xslFileList;

    protected JRSTView jrstView;

    public JRSTCommandModel(JRSTView jrstView) {
        this.jrstView = jrstView;
        xslFileList = Lists.newArrayList();
        selectedFormat = "xml";
    }

    public Boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(Boolean overwrite) {
        Boolean oldValue = isOverwrite();
        this.overwrite = overwrite;
        firePropertyChange("overwrite", oldValue, overwrite);
    }

    public Boolean isSimpleMode() {
        return simpleMode;
    }

    public void setSimpleMode(Boolean simpleMode) {
        Boolean oldValue = isSimpleMode();
        this.simpleMode = simpleMode;
        firePropertyChange("simpleMode", oldValue, simpleMode);
    }

    public Boolean isFormatEnabled() {
        return formatEnabled;
    }

    public void setFormatEnabled(Boolean formatEnabled) {
        Boolean oldValue = isFormatEnabled();
        this.formatEnabled = formatEnabled;
        firePropertyChange("formatEnabled", oldValue, formatEnabled);
    }

    public File getSaveFile() {
        return saveFile;
    }

    public void setSaveFile(File saveFile) {
        File oldValue = getSaveFile();
        this.saveFile = saveFile;
        firePropertyChange("saveFile", oldValue, saveFile);
    }

    public File getOpenFile() {
        return openFile;
    }

    public void setOpenFile(File openFile) {
        File oldValue = getOpenFile();
        this.openFile = openFile;
        firePropertyChange("openFile", oldValue, openFile);
    }

    public String getSelectedFormat() {
        return selectedFormat;
    }

    public void setSelectedFormat(String selectedFormat) {
        String oldValue = getSelectedFormat();
        this.selectedFormat = selectedFormat;
        firePropertyChange("selectedFormat", oldValue, selectedFormat);
    }

    public List<File> getXslFileList() {
        return xslFileList;
    }

    public void setXslFileList(List<File> xslFileList) {
        List<File> oldValue = getXslFileList();
        this.xslFileList = xslFileList;
        firePropertyChange("xslFileList", oldValue, xslFileList);
    }

    public String getXsls() {
        String xslAsString = StringUtil.join(getXslFileList(), new StringUtil.ToString<File>() {

            @Override
            public String toString(File file) {
                return file.getAbsolutePath();
            }
        }, ",", true);
        return xslAsString;
    }

    public void addXslFile(File xslFile, int index) {
        List<File> oldValue = getXslFileList();
        xslFileList.add(index, xslFile);
        firePropertyChange("xslFileList", oldValue, xslFileList);
    }

    public File getXslFile(Integer number) {
        if (xslFileList.size() <= number) {
            return null;
        }
        return xslFileList.get(number);
    }

    public int getPanelNumber() {
        return xslFileList.size();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }
}
