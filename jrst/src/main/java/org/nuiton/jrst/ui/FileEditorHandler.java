/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2008 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.ui;

import com.google.common.io.Files;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * @author sletellier
 */
public class FileEditorHandler {

    public static final String SEPARATOR_REGEX = "\\s*,\\s*";
    protected FileEditor view;

    public FileEditorHandler(FileEditor view) {
        this.view = view;
    }

    public void openLocation() {

        // use last selected file
        File startFile = view.getSelectedFile();
        String startPath = view.getStartPath();
        if (startFile == null && startPath != null) {


            // else filed start path
            startFile = new File(startPath);
        } else {

            // else start with user home
            startFile = new File(System.getProperty("user.home"));
        }
        JFileChooser fc = new JFileChooser(startFile);

        fc.setDialogTitle(view.getTitle());
        fc.setAcceptAllFileFilterUsed(view.getAcceptAllFileFilterUsed());

        // TODO sletellier 14/06/2012 : activate multi selection
//        boolean multiSelectionEnabled = view.isMultiSelectionEnabled();
//        fc.setMultiSelectionEnabled(multiSelectionEnabled);

        // used to enable directory selection
        boolean directoryEnabled = view.isDirectoryEnabled();
        if (directoryEnabled) {
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }

        // used to enable file selection
        boolean fileEnabled = view.isFileEnabled();
        if (fileEnabled) {

            if (directoryEnabled) {

                // both
                fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            } else {
                fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            }
            String extsAsString = view.getExts();
            if (extsAsString != null) {

                // extentions can be separted by comma
                String[] exts = extsAsString.split(SEPARATOR_REGEX);
                String extsDescription = view.getExtsDescription();

                String[] descs = new String[0];
                if (extsDescription != null) {
                    descs = extsDescription.split(SEPARATOR_REGEX);
                }
                for (int i = 0;i<exts.length;i++) {

                    // use ext if no desc found
                    String ext = exts[i];
                    String desc = ext;
                    if (descs.length > i) {
                        desc = descs[i];
                    }

                    fc.addChoosableFileFilter(new ExtentionFileFiler(ext, desc));
                }
            }
        }

        // directory or/and file must be enabled
        if (!directoryEnabled && !fileEnabled) {
            throw new IllegalArgumentException("You must enable at least file or directory to open dialog");
        }

        // show dialog
        int returnVal = fc.showOpenDialog(view);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            // get selected to display in ui
            File file = fc.getSelectedFile();

            view.setSelectedFile(file);
        }
    }

    public static class ExtentionFileFiler extends FileFilter {
        protected String ext;
        protected String desciption;

        public ExtentionFileFiler(String ext, String desciption) {
            this.ext = ext;
            this.desciption = desciption;
        }

        @Override
        public boolean accept(File file) {
            if (file.isDirectory()) {
                return true;
            }
            String fileExtension = Files.getFileExtension(file.getName());
            return ext.equals(fileExtension);
        }

        @Override
        public String getDescription() {
            return desciption;
        }
    }
}
