/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2022 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.component.annotations.Component;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.nuiton.jrst.legacy.JRSTReader;
import org.python.util.PythonInterpreter;
import org.xml.sax.InputSource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * Old mecanism to transform rst file to xml format using {@link JRSTReader}.
 *
 * @author tchemit (chemit@codelutin.com)
 * @since 2.0.1
 */
@Component(role = JRSTToXmlStrategy.class, hint = "docutils",
           description = "Transform a RST model (using jython + docutils), " +
                         "to a xml format.")

public class JRSTToXmlStrategyDocutils implements JRSTToXmlStrategy {

    private static final String DOCUTILS_LAUNCHER = "__run__.py";

    private static final String IMPORT_SCRIPT = "import __run__";

    private static final String WINDOWS_NAME = "win";

    private static final String OS_NAME = "os.name";

    private static final String BANG = "!";

    private static final String FILE_URI_PREFIX = "file:";

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(JRSTToXmlStrategyDocutils.class);

    @Override
    public Document generateRstToXml(File in, String encoding) throws Exception {
        ByteArrayOutputStream out = null;

        try {
            // Transformation to XML
            out = new ByteArrayOutputStream();

            // Transformation of the __run__ URL into a path that python will use
            // For example the URL is :
            // jar:file:/home/user/.m2/repository/org/nuiton/jrst/docutils/1.6-SNAPSHOT/docutils-1.6-SNAPSHOT.jar!/__run__.py
            // and it becomes :
            // /home/user/.m2/repository/org/nuiton/jrst/docutils/1.6-SNAPSHOT/docutils-1.6-SNAPSHOT.jar/
            URL resource = JRST.class.getResource("/" + DOCUTILS_LAUNCHER);
            String docutilsPath = resource.getPath()
                    .replaceAll(DOCUTILS_LAUNCHER, "");

            docutilsPath = docutilsPath.replaceAll(BANG, "");
            docutilsPath = docutilsPath.replaceAll(FILE_URI_PREFIX, "");

            // Import of the main script to use docutils ( __run__ )
            PythonInterpreter interp = new PythonInterpreter();
            String commandImport = IMPORT_SCRIPT;
            interp.exec(commandImport);

            // If the OS is windows, escapes the backslashs in the filepath
            String filePath = in.getAbsolutePath();
            String property = System.getProperty(OS_NAME).toLowerCase();
            if (property.contains(WINDOWS_NAME)) {
                filePath = filePath.replaceAll("\\\\", "\\\\\\\\");
            }

            // Sets an output stream in the python interpreter and executes the code
            interp.setOut(out);

            // Execution of the docutils script to transform rst to xml
            String commandExec = String.format("__run__.exec_docutils('%s', '%s', '%s')",
                                               docutilsPath, JRST.TYPE_XML, filePath);
            interp.exec(commandExec);

            // Cleans the python interpreter to avoid problems if they are multiple execution of this method
            interp.cleanup();

            // Transforms the output stream to a document
            String xmlString = out.toString(encoding);

            Document doc = null;
            try {
                doc = parseText(xmlString);
            } catch (DocumentException e) {
                log.error("Error during the creation of the document", e);
            }
            out.close();

            return doc;
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    // copied and adapted from DocumentHelper#parseText(String)
    protected Document parseText(String text) throws DocumentException {
        SAXReader reader = new SAXReader();
        reader.setEntityResolver(new JRSTEntityResolver());
        InputSource source = new InputSource(new StringReader(text));
        String encoding = getEncoding(text);
        source.setEncoding(encoding);

        Document result = reader.read(source);
        if (result.getXMLEncoding() == null) {
            result.setXMLEncoding(encoding);
        }
        return result;
    }

    // copied from DocumentHelper#getEncoding(String)
    private static String getEncoding(String text) {
        String result = null;

        String xml = text.trim();

        if (xml.startsWith("<?xml")) {
            int end = xml.indexOf("?>");
            String sub = xml.substring(0, end);
            StringTokenizer tokens = new StringTokenizer(sub, " =\"\'");

            while (tokens.hasMoreTokens()) {
                String token = tokens.nextToken();

                if ("encoding".equals(token)) {
                    if (tokens.hasMoreTokens()) {
                        result = tokens.nextToken();
                    }

                    break;
                }
            }
        }

        return result;
    }
}
