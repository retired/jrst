/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Test sur les elements de type text (liens, styles...)
 * 
 * @author chatellier
 */
public class TextTest extends JRSTAbstractTest {

    /**
     * Test que les liens avec parametres sont correctement parsés.
     */
    @Test
    public void testLinksParameters() throws Exception {
        File in = getBugTestFile("testLinks.rst");
        File out = getOutputTestFile("jrst-testLinks.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("href=\"http://labs.libre-entreprise.org/tracker/?atid=113&amp;group_id=8&amp;func=browse\"") > 0);
                assertTrue(content.indexOf("href=\"http://www.docbook.org/\"") > 0);
                assertTrue(content.indexOf("href=\"http://www.docbook2.org/\"") > 0);
            }
        };
    }

    /**
     * Test que les caracteres speciaux dans les label des liens
     * ne sont pas perdus.
     */
    @Test
    public void testLinksSpecialCharacters() throws Exception {
        File in = getBugTestFile("testLinks1380.rst");
        File out = getOutputTestFile("jrst-testLinks.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("nuiton's forge") > 0);
                assertTrue(content.indexOf("build.xml") > 0);
            }
        };
    }

    /**
     * Test que la presence de caractere "tabulation" ne cause
     * pas d'exception lors de la generation.
     */
    @Test
    public void testTabCharacter() throws Exception {
        File in = getBugTestFile("testTab1378.rst");
        File out = getOutputTestFile("jrst-testTab1378.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                Pattern pattern = Pattern.compile(".*<blockquote>.*du bla bla \u00E9l\u00E9mentaire.*</blockquote>.*", Pattern.DOTALL);
                Matcher matcher = pattern.matcher(content);
                assertTrue(matcher.find());
            }
        };
    }

    /**
     * Test que les synthaxe options sont bien parsées.
     */
    @Test
    public void testOptionLists() throws Exception {
        File in = getBugTestFile("testOptionsList644.rst");
        File out = getOutputTestFile("jrst-testOptionList644.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("<span class=\"option\">-a</span>") > 0);
                assertTrue(content.indexOf("<p>options can have arguments\nand long descriptions</p>") > 0
                        ||content.indexOf("<p>options can have arguments and long descriptions</p>") > 0);
                assertTrue(content.indexOf("&lt;v3region.zip|v2region.xml|v2region.xml.gz&gt;") > 0);
                assertTrue(content.indexOf("<span class=\"option\">/V</span>") > 0);
                assertTrue(content.indexOf("<p>DOS/VMS-style options too</p>") > 0);
            }
        };
    }
    
    /**
     * Test que les echapements de lien fonctionne.
     */
    @Test
    @Ignore
    public void testLinkEscape() throws Exception {
        File in = getBugTestFile("testLinks.rst");
        File out = getOutputTestFile("jrst-testLinks.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("echapement de lien1_") > 0);
                assertTrue(content.indexOf("echapement de *.txt") > 0);
            }
        };
    }
    
    /**
     * Test embedded links.
     */
    @Test
    public void testEmbededURIs() throws Exception {
        File in = getBugTestFile("testEmbeddedURIs.rst");
        File out = getOutputTestFile("jrst-testEmbeddedURIs.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.contains("href=\"http://www.python.org\""));
                assertTrue(content.contains("href=\"./python\""));
                assertTrue(content.contains("href=\"http://www.rfc-editor.org/rfc/rfc2396.txt\""));
                assertTrue(content.contains("href=\"http://www.rfc-editor.org/rfc/rfc2732.txt\""));
            }
        };
    }

}
