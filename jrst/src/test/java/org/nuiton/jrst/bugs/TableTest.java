/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2011 - 2017 Codelutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;

/**
 * Test concernant les tables en général.
 * 
 * @author chatellier
 */
public class TableTest extends JRSTAbstractTest {
    
    /**
     * Test que la présence d'un caractères | ne fait pas planter
     * la generation des tables.
     */
    @Test
    public void testTableSpecialChar() throws Exception {
        File in = getBugTestFile("testTable1290.rst");
        File out = getOutputTestFile("jrst-tables.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("Annee|Trait|Espece") > 0);
                assertTrue(content.indexOf("<p>Fatal</p>") > 0);
            }
        };
    }

    /**
     * Test que la generation de tableau ne genere pas de paragraphe en trop.
     * 
     * @throws Exception
     */
    @Test
    public void testTableIndentation() throws Exception {
        File in = getBugTestFile("testTable1375.rst");
        File out = getOutputTestFile("jrst-tables.html");

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
                assertTrue(content.indexOf("<em>légérs</em>") > 0);
            }
        };
    }
}
