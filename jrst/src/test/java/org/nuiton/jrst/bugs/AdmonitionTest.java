/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;

/**
 * Test concernant les admonitions.
 *
 * @author sletellier
 */
public class AdmonitionTest extends JRSTAbstractTest {

    /**
     * Test que les options des admonitions dans des listes sont bien parsée
     * parsées.
     */
    @Ignore
    @Test
    public void testAdmonitionInList() throws Exception {
        File in = getBugTestFile("testAdminitionInList1787.rst");
        File out = getOutputTestFile("jrst-testAdminitionInList1787.html");
//        out.deleteOnExit();

        new JRSTTestGenerator(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME) {

            @Override
            public void assertJRST(File in, File out) throws Exception {
                String content = FileUtils.readFileToString(out, JRST.UTF_8);
            //      Must contains <div class="note">
                assertTrue(content.contains("<div class=\"note\">"));
            }
        };
    }

}
