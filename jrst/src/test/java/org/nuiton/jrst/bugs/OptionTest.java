/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2011 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.bugs;

import java.io.File;

import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

/**
 * Test concernant les options.
 *
 * @author sletellier
 */
public class OptionTest extends JRSTAbstractTest {

    /**
     * Test que les options des options dans des listes sont bien parsée
     * parsées.
     */
    @Test
    public void testOptionArgumentSizeInList() throws Exception {
        File in = getBugTestFile("testOptionArgumentSize1788.rst");
        File out = getOutputTestFile("jrst-testOptionArgumentSize1788.html");
        generate(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME);
    }

}
