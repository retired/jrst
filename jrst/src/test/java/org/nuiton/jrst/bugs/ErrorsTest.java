/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2012 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jrst.bugs;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.nuiton.jrst.JRST;
import org.nuiton.jrst.JRSTAbstractTest;

import java.io.File;

/**
 * Test about the display of warning or errors in the generated site
 *
 * @author jpages
 */
public class ErrorsTest extends JRSTAbstractTest {

    /**
     * Tests if rst warnings or errors are displayed in the generated html
     */
    @Test
    public void testDisplayErrors() throws Exception {
        File in = getBugTestFile("testDisplayErrors.rst");
        File out = getOutputTestFile("jrst-testDisplayErrors.html");

        JRST.generate(JRST.TYPE_HTML, in, out, JRST.Overwrite.ALLTIME, false);
        String content = FileUtils.readFileToString(out, JRST.UTF_8);

        assertTrue(content.contains("Explicit markup ends without a blank line; unexpected unindent."));
        assertTrue(content.contains("Literal block ends without a blank line; unexpected unindent."));
        assertTrue(content.contains("Inline literal start-string without end-string."));
        assertTrue(content.contains("Duplicate target name, cannot be used as a unique reference: " +
                "\"dossier contenant tous les fichiers nécessaires à ce plan\"."));
    }
}
