/*
 * #%L
 * JRst :: Api
 * %%
 * Copyright (C) 2004 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jrst;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;

/**
 * Class qui redirige la sortie standard pour la laisser en interne
 */
public class ThreadRedirection extends Thread {

    public static final String LINE_SEPARATOR = "\n";
    protected StringBuffer str;
    protected Process process;

    public ThreadRedirection(Process p) {
        process = p;
        str = new StringBuffer();
    }

    @Override
    public void run() {
    	BufferedReader reader =null;
        try {
        	
            reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()),8193);
            
                String line;
                while ((line = reader.readLine()) != null) {
                	
                	/*Le doctype est modifie pour eviter que sax telecharge la DTD du HTML sur le net, car le traitement prend un certain temps.
                	    On doit en revanche spécifier la signification de nbsp.
                	*/
                	
                    if(!line.startsWith("<!DOCTYPE html "))
                        {
                        str.append(line + LINE_SEPARATOR);
                        }
                    else {
                       str.append("<!DOCTYPE xsl:stylesheet [<!ENTITY nbsp '&#160;'>]> " + LINE_SEPARATOR);
                    }
                }

            
            }
        catch (IOException ioe) {
            ioe.printStackTrace();
        } 
        finally {
           IOUtils.closeQuietly(reader);}
    }

    public String getOutput() {
        return str.toString();
    }
}
