.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2012 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
.. -*- coding: utf-8 -*-

Acknowledgements
================

:Author: David Goodger
:Contact: goodger@python.org
:Date: $Date$
:Revision: $Revision$
:Copyright: This document has been placed in the public domain.

I would like to acknowledge the people who have made a direct impact
on the Docutils project, knowingly or not, in terms of encouragement,
suggestions, criticism, bug reports, code contributions, cash
donations, tasty treats, and related projects:

* Aahz
* David Abrahams
* Guy D. Alcos
* David Ascher
* Ned Batchelder
* Heiko Baumann
* Anthony Baxter
* Eric Bellot
* Frank Bennett
* Ian Bicking
* Marek Blaha
* Martin Blais
* Stephen Boulet
* Fred Bremmer
* Simon Budig
* Bill Bumgarner
* Brett Cannon
* Greg Chapman
* Nicolas Chauveau
* Beni Cherniavsky
* Adam Chodorowski
* Brent Cook
* Laura Creighton
* Artur de Sousa Rocha
* Stephan Deibel & `Wing IDE <http://wingide.com/>`__
* Jason Diamond
* William Dode
* Fred Drake
* Reggie Dugard
* Dethe Elza
* Marcus Ertl
* Benja Fallenstein
* fantasai
* Stefane Fermigier
* Michael Foord
* Jim Fulton
* Peter Funk
* Lele Gaifax
* Dinu C. Gherman
* Matt Gilbert
* Jorge Gonzalez
* Engelbert Gruber
* Jacob Hallen
* Simon Hefti
* Doug Hellmann
* Marc Herbert
* Juergen Hermann
* Jannie Hofmeyr
* Steve Holden
* Michael Hudson
* Marcelo Huerta San Martin
* Ludger Humbert
* Jeremy Hylton
* Tony Ibbs
* Alan G. Isaac
* Alan Jaffray
* Joe YS Jaw
* Dmitry Jemerov
* Richard Jones
* Andreas Jung
* Robert Kern
* Garth Kidd
* Philipp KnÃ¼sel
* Axel Kollmorgen
* Jeff Kowalczyk
* Martin F. Krafft
* Meir Kriheli
* Dave Kuhlman
* Lloyd Kvam
* Kirill Lapshin
* Nicola Larosa
* Daniel Larsson
* Marc-Andre Lemburg
* Julien Letessier
* Chris Liechti
* Wolfgang Lipp
* Edward Loper
* Dallas Mahrt
* Mikolaj Machowski
* Ken Manheimer
* Bob Marshall
* Mark McEahern
* Vincent McIntyre
* John F Meinel Jr
* Ivan Mendez for Free Software Office of the University of A CoruÃ±a
* Eric Meyer
* GÃ¼nter Milde
* Skip Montanaro
* Paul Moore
* Nigel W. Moriarty
* Hisashi Morita
* Mark Nodine
* Omidyar Network (Pierre Omidyar & Doug Solomon)
* Panjunyong
* Patrick K. O'Brien
* Michel Pelletier
* Sam Penrose
* Tim Peters
* Pearu Peterson
* Martijn Pieters
* Mark Pilgrim
* Brett g Porter
* David Priest
* Jens Quade
* Stefan Rank
* Edward K. Ream
* Andy Robinson
* Tavis Rudd
* Tracy Ruggles
* Oliver Rutherfurd
* Luc Saffre
* Seo Sanghyeon
* Kenichi Sato
* Ueli Schlaepfer
* Gunnar Schwant
* Bill Sconce
* Frank Siebenlist
* Bruce Smith
* Nir Soffer
* Asko Soukka
* Darek Suchojad
* Roman Suzi
* Janet Swisher
* tav
* Kent Tenney
* Bob Tolbert
* Paul Tremblay
* Laurence Tratt
* Adrian van den Dries
* Guido van Rossum
* Miroslav Vasko
* Paul Viren
* Martin von Loewis
* Greg Ward
* Barry Warsaw
* Wu Wei
* Edward Welbourne
* Lea Wiemann
* Anthony Williams
* Robert Wojciechowicz
* Ka-Ping Yee
* Moshe Zadka
* Shmuel Zeigerman

Thank you!

Special thanks to `SourceForge <http://sourceforge.net>`__ and the
`Python Software Foundation <http://www.python.org/psf/>`__.

Hopefully I haven't forgotten anyone or misspelled any names;
apologies (and please let me know!) if I have.
