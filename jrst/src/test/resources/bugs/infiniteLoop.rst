.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Developer guide for working with Mifos internationalization, specifically as relates to making translation files work with the online translation tool.

Goals
=====

* strictly limit the proliferation of resource bundles to ensure manageability
* support an extensible translatable string dictionary format to make life easy for translators

Overall i18n Architecture
=========================

Following `the Cheetah i18n architecture guide <CheetahInternationalization>`_, "master" keys and translatable strings are in Java ``.properties`` format resource bundles. These ``.properties`` files will not contain underscores in their filenames. These files will be stored in source code version control.

Language-specific translations will also be stored in source code version control, but only in ``.po`` (portable object) format, like ``LOCALE/bundlename_LOCALE.po``. Language-specific Java ``.properties`` format resource bundles needed by Mifos to display the application in a language other than the default will be generated during the build.

Notes
=====

Things are still in development right now - we would like everything to run automatically from the maven build, but there are various shell scripts in the process right now. We're working to make this process smoother!

Required Software
=================

* `Translate Toolkit <http://translate.sourceforge.net/wiki/toolkit>`_ - `TT install page <http://translate.sourceforge.net/wiki/toolkit/installation>`_ (required on all platforms. On Windows we suggest using the .exe installer)
* `Cygwin <http://www.cygwin.org/cygwin/>`_ (required on Windows, to run the shell script)
* On Mac OS, required python module lxml may not be installed by default. See installation instructions `here <http://codespeak.net/lxml/installation.html>`_.

General Guidelines
==================

* Before working with translation files, perform an update to pull the latest changes in source control into your working copy.
* Use detailed log messages when committing. Mention *why* you made the change, refer to related issues, and link to related mailing list threads.
* If you are doing anything directly on the .properties files, please notify the PM managing Online Translations as this might affect any existing translation work being done.
* When operating directly on .po or .properties files (not via the online translation tool) watch for conflicts on the online translation tool server's working copy.

Converting legacy bundles
=========================

Create the directory to store .po files (for example, "fr") in mifos/src/org/mifos/config/localizedResources. Use ``capture_initial_locales.sh`` to convert legacy Java .properties-based resource bundles to .po format.

Follow with ``locale_sync.sh`` to clean up duplicate msgctxt strings and add fuzzy translations.

Adding new keys
===============

* add key and default translation to appropriate ``.properties`` file
* run ``locale_sync.sh``

  * On Windows, running from the Cygwin command, if you get errors like ``./locale_sync.sh: line 3: $'\r': command not found`` then run ``dos2unix local_sync.sh`` and try rerunning it.
  * Also note before running ./locale_sync.sh in Cygwin you need to get to the windows directory your localized resource files are in.  An example of doing this on C drive is:
    cd /cygwin/c/mifosinstallation/mifos-gazelle/application/src/main/resources/org/mifos/config/localizedResources
  * Note that currently this will update all ``.po`` files, even if no changes have been made to the corresponding ``.properties`` file.  In the future this could be improved so that only the ``.po`` files affected by the new key were updated.
* commit default ``.properties`` and ``LOCALE/*.po`` files

New ``.properties`` files will also be picked up by ``locale_sync.sh``, but in general new keys should be added to existing ``.properties`` files.

Care should be taken to only add new default/master translations and translation files when absolutely necessary (see `Goals`_).  If you need some text, first check and see if it is already in a ``.properties`` file.

Some things to note:

* Only the English (default) version of the .properties file should be translated.  (As of this writing, non-English .properties files are still in version control, but they will be eliminated since they are generated from .PO files)
* Running local_sync.sh requires the external software mentioned above.  In the future, we plan to move the work performed by this script into the maven build in order to eliminate this step and its tool requirements.

Adding new languages
====================

* Locales are either made up of a single language code (for instance, ``en``, for English language), or a combination of of a language code and a country code (``en_US``, for United States English). (Here is the `Sun Java Locale page <http://java.sun.com/developer/technicalArticles/J2SE/locale/>`_ which has more info.)

  * Find the two-letter language code using the `ISO 639-2 language code list <http://www.loc.gov/standards/iso639-2/php/code_list.php>`_.
  * If the locale has a country code, find the two-letter country code in the `ISO 3166 country code list <http://www.iso.org/iso/country_codes/iso_3166_code_lists/english_country_names_and_code_elements.htm>`_

* create directory using the code for locale.
* run ``locale_sync.sh``
* commit ``LOCALE/*.po`` files
* a Pootle administrator also needs to add new translation files for this language to the Pootle server via the Pootle admin UI; make a request on the `mifos-developer mailing list </developers/listserv#developer-listserv>`_ for this.

  * If you are the Pootle administrator, on the server that Pootle runs on, do this:

    * cd to ``/var/lib/pootle/gazelle``
    * ``sudo sudo -u pootle svn co https://mifos.svn.sourceforge.net/svnroot/mifos/trunk/application/src/main/resources/org/mifos/config/localizedResources/km km --username mifospootle`` (replacing km with the locale name you have added) (You will need the mifospootle password; ask one of the developers if you don't have this.)
    * restart the Pootle server: ``sudo /etc/init.d/pootle restart``
* Enable the locale in the Mifos software by following AddingANewLocale


Making a build with language bundles
====================================

Property file bundles for non-English languages are generated by the ``create_props.sh`` script.

* To generate a build with all language bundles, cd to the ``/mifos/src/main/resources/org/mifos/config/localizedResources`` directory, and run the ``create_props.sh`` script. This will generate all the locale-specific property files from the .po files.
* The Hudson job for gazelle trunk runs this script automatically.
* We would like to eliminate the shell scripts. One option is the `Maven 2.x Gettext Plugin <http://xnap-commons.sourceforge.net/m2/maven-gettext-plugin/index.html>`_.

Bulk Updates
============

On neem:

::

  sudo su - pootle
  svn update /var/lib/pootle/gazelle/*

Other operations can be performed as the pootle user, such as manually editing .po files and resolving conflicts.
