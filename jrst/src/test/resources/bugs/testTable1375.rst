.. -
.. * #%L
.. * JRst :: Api
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
==================
Spécifications 2.0
==================

Titre élémentaire
=================

+--------------------------+--------------------------+------------------------+
| La séparation du         | L'utilisation du         | L'utilisation d'un     |
| fond de la forme         | format plain text        | gestionnaire de version|
+==========================+==========================+========================+
| Nous ne perdons plus de  | Possibilité d'utiliser un| Historique fin des     |
| temps sur la mise en     | outil de gestion de      | versions.              |
| forme et pouvons nous    | versions tel que Git ou  |                        |
| concentrer sur le fond.  | Mercurial pour un        | Diff aisés et lisibles |
|                          | versionning puissant des | entre différentes      |
| Plus besoin de faire de  | documents.               | versions.              |
| fastidieuses mises en    |                          |                        |
| page.                    | Utilisation de notre     | Travail collaboratif   |
|                          | traitement de textes     | facilité grâce aux     |
| Plus de style cassés ou  | *légérs* préféré.        | merges automatiques.   |
| de numérotations         |                          |                        |
| discontinues comme on les| Fichiers légers.         | Travail isolé dans des |
| retrouve si souvent avec |                          | branches.              |  
| notre éditeur de texte   |                          |                        |
| préféré : Word.          |                          |                        |
+--------------------------+--------------------------+------------------------+
